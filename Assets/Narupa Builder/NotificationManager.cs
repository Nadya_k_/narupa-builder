﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

public class NotificationManager : MonoBehaviour
{
    private ulong handle = 0;

    private void Start()
    {
        OpenVR.Overlay.CreateOverlay(Guid.NewGuid().ToString(),
                                     "Narupa Builder",
                                     ref handle);
    }

    private uint currentNotification = 0;

    [Header("Events")]
    [SerializeField]
    private UnityEvent onPositiveNotification;

    [SerializeField]
    private UnityEvent onNegativeNotification;

    public void ShowNegativeNotification(string message)
    {
        ShowNotification(message);
        onNegativeNotification?.Invoke();
    }

    public void ShowPositiveNotification(string message)
    {
        ShowNotification(message);
        onPositiveNotification?.Invoke();
    }

    public void ShowNotification(string message, bool persistent = false)
    {
        var bitmap = new NotificationBitmap_t();
        if (currentNotification > 0)
            OpenVR.Notifications.RemoveNotification(currentNotification);
        var type = persistent ? EVRNotificationType.Persistent : EVRNotificationType.Transient;
        OpenVR.Notifications.CreateNotification(handle,
                                                0,
                                                type,
                                                message,
                                                EVRNotificationStyle.Application,
                                                ref bitmap,
                                                ref currentNotification);
    }

    public void ShowStatus(string message)
    {
        ShowNotification(message, true);
    }
}