using System.Threading.Tasks;

namespace NarupaBuilder
{
    /// <summary>
    /// A source for an <see cref="IFragment" /> which provides it asynchronously.
    /// </summary>
    public interface IFragmentProvider
    {
        /// <summary>
        /// Load the fragment asynchronously, for example from a file or web address.
        /// </summary>
        /// <remarks>
        /// It is expected that the
        /// <see cref="IFragmentProvider" />
        /// caches the result, such that further calls to
        /// <see cref="GetFragmentAsync" /> are instantaneous.
        /// </remarks>
        Task<Fragment> GetFragmentAsync();

        /// <summary>
        /// The name of the fragment. This may change once <see cref="GetFragmentAsync" />
        /// has been called.
        /// </summary>
        string Name { get; }
    }
}