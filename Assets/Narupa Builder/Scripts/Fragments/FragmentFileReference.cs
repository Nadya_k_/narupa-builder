using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// Fragment which loads a fragment from a filename when first requested.
    /// </summary>
    public class FragmentFileReference : IFragmentProvider
    {
        public FragmentFileReference(string filename)
        {
            Filename = filename;
        }

        private string Filename { get; }

        private Fragment cachedFragment;

        /// <inheritdoc cref="IFragmentProvider.GetFragmentAsync" />
        public Task<Fragment> GetFragmentAsync()
        {
            if (cachedFragment == null)
            {
                var system = ImportExport.Import(Filename);
                system.RecenterParticles(Vector3.zero);
                cachedFragment = new Fragment(Name, system);
            }

            return Task.FromResult(cachedFragment);
        }

        /// <inheritdoc cref="IFragmentProvider.Name" />
        public string Name => Path.GetFileNameWithoutExtension(Filename);
    }
}