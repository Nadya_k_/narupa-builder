// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Frontend.UI;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class CurrentPointerTooltip : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI text;

        private string defaultText = "";

        public void SetDefaultText(string text)
        {
            defaultText = text;
        }

        private void Awake()
        {
            text.text = defaultText;
        }

        private void Update()
        {
            text.text = ((EventSystem.current.currentInputModule as NarupaInputModule)
                            .CurrentHoverTarget)?.name ?? defaultText;
        }
    }
}