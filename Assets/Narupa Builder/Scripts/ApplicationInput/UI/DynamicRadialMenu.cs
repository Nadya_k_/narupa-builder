// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    /// <summary>
    /// Script for arranging children radially around a point
    /// </summary>
    public class DynamicRadialMenu : UIBehaviour
    {
        [SerializeField]
        private GameObject radialPrefab;

        [SerializeField]
        private float angularOffset = 0;

        public GameObject AddItem(Sprite image, string name, Action callback)
        {
            var gameObject = Instantiate(radialPrefab, this.transform);
            var button = gameObject.transform.Find("Button").GetComponent<Button>();
            button.name = name;
            button.transform.Find("Image").GetComponentInChildren<Image>().sprite = image;
            button.onClick.AddListener(() => callback());
            ArrangeChildrenRadially();
            return gameObject;
        }
        
        private void ArrangeChildrenRadially()
        {
            var childCount = transform.childCount;
            var da = 360f / childCount;
            for (var i = 0; i < childCount; i++)
            {
                transform.GetChild(i).transform.localRotation =
                    Quaternion.Euler(0, 0, angularOffset - da * i);

                // Rotate icon to be right way up
                var button = transform.GetChild(i).GetComponentInChildren<Button>();
                var image = button.transform.Find("Image").GetComponentInChildren<Image>();

                image.transform.localRotation = Quaternion.Euler(0, 0, -(angularOffset - da * i));
            }
        }
    }
}