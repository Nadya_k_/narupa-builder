// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Narupa_Builder.Scripts.ApplicationInput.UI;

namespace NarupaBuilder.Tools
{
    public class HoverMenuModal : Modal
    {
        public HoverMenuModal(ActionsManager manager, Action onModalEnded) : base(manager)
        {
            ModalClosed += onModalEnded;
        }

        public HoverCanvas Ui { get; set; }

        public override bool CanBeReplacedBy(IModal newModal)
        {
            return !(newModal is HoverMenuModal);
        }

        public void Click()
        {
            Ui?.Click();
        }

        public override void OnCloseModal()
        {
            Ui?.DestroyMenu();
        }

        public override bool HideMolecule => true;
    }
}