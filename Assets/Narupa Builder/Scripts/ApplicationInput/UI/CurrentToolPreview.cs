// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NarupaBuilder.Tools
{
    public class CurrentToolPreview : UIBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI toolName;

        [SerializeField]
        private Image toolImage;

        [SerializeField]
        private ToolManager toolManager;

        [SerializeField]
        private ActionsManager actionsManager;

        [SerializeField]
        private CanvasGroup canvasGroup;

        protected override void Start()
        {
            base.Start();
            toolManager.ToolChanged += ToolChanged;
            ToolChanged();
        }

        private void ToolChanged()
        {
            var tool = toolManager.CurrentTool;
            DisplayTool(tool, toolName, toolImage);
        }

        private void Update()
        {
            canvasGroup.alpha = actionsManager.IsActionAllowed ? 1f : 0f;
        }

        private void DisplayTool(ITool tool, TMP_Text name, Image image)
        {
            name.enabled = tool != null;
            image.enabled = tool != null;
            if (tool != null)
            {
                name.text = tool.DisplayName;
                image.sprite = tool.SpriteIcon;
            }
        }
    }
}