using System;
using Narupa.Frontend.Controllers;
using Narupa.Frontend.Input;
using Narupa.Frontend.UI;
using Narupa.Frontend.XR;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Narupa_Builder.Scripts.ApplicationInput.UI
{
    [RequireComponent(typeof(Canvas))]
    public class HoverCanvas : MonoBehaviour
    {
        /// <summary>
        /// Instantiate a copy of the provided prefab at the location of the cursor, overriding the
        /// input of the UI to use this canvas.
        /// </summary>
        public static HoverCanvas Instantiate(HoverCanvas menuPrefab, IPosedObject cursor)
        {
            var menu = Instantiate(menuPrefab);
            menu.Setup(cursor);
            return menu;
        }

        private void Setup(IPosedObject cursor)
        {
            if (cursor == null)
                throw new InvalidOperationException(
                    "PhysicalRadialMenu created without a physical cursor!");

            var canvas = GetComponent<Canvas>();
            
            WorldSpaceCursorInput.SetCanvasAndCursor(canvas,
                                                     cursor,
                                                     new DirectButton());
            
            // Position menu at cursor
            canvas.transform.position = cursor.Pose.Value.Position;

            // Rotate menu to face camera, but keep world up direction.
            canvas.transform.rotation = Quaternion.LookRotation(
                (canvas.transform.position - Camera.main.transform.position).normalized, Vector3.up);
        }

        public void DestroyMenu()
        {
            WorldSpaceCursorInput.UnregisterCanvas();
            Destroy(gameObject);
        }

        public void Click()
        {
            var hovered = (EventSystem.current.currentInputModule as NarupaInputModule)
                .CurrentHoverTarget;
            if (hovered != null)
            {
                ExecuteEvents.Execute(hovered, new BaseEventData(EventSystem.current),
                                      ExecuteEvents.submitHandler);
            }
        }
    }
}