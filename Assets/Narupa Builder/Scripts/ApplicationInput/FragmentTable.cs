// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Frontend.Controllers;
using NarupaBuilder.Tools;
using UnityEngine;
using Valve.VR;

namespace NarupaBuilder
{
    public class FragmentTable : MonoBehaviour
    {
        [SerializeField]
        private MoleculePreview previewPrefab;

        private List<MoleculePreview> previews = new List<MoleculePreview>();

        [SerializeField]
        private VrController controller;

        [SerializeField]
        private ActionsManager actionsManager;

        [SerializeField]
        private float maxCursorDistance = 0.1f;

        [SerializeField]
        private SteamVR_Action_Boolean selectFragmentAction;

        [SerializeField]
        private SteamVR_Input_Sources inputSources;

        [SerializeField]
        private SteamVR_Action_Boolean exitFragmentTableAction;

        public void Setup(ActionsManager am,
                          IReadOnlyCollection<IFragmentProvider> fragments)
        {
            actionsManager = am;
            controller = am.activeController.Controller;

            modal = new FragmentTableModal(actionsManager, this);

            actionsManager.SetModal(modal);
            actionsManager.CancelCurrentAction();

            selectFragmentAction.AddOnStateUpListener(OnSelectFragment, inputSources);
            exitFragmentTableAction.AddOnStateUpListener(OnExitFragment, inputSources);

            var playerLocation = Camera.main.transform.position;

            var forward = Camera.main.transform.forward;
            forward.y = 0;
            forward = forward.normalized;

            this.transform.position = playerLocation - forward * focalLength / 2f;

            this.transform.rotation = Quaternion.LookRotation(forward, Vector3.up);

            var totalAngle = (width - 1) * angularSpacing;

            var x = 0;
            var y = 0;

            var count = fragments.Count;
            var rows = (int) Mathf.Floor((count - 1) / width);

            foreach (var fragment in fragments)
            {
                var preview = Instantiate(previewPrefab, transform);

                preview.SetFragment(fragment);

                previews.Add(preview);

                var angle = x * angularSpacing - totalAngle / 2f;


                preview.transform.localPosition +=
                    new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad) * focalLength,
                                verticalSpacing * (y - 1 - rows / 2f),
                                Mathf.Cos(angle * Mathf.Deg2Rad) * focalLength);

                var personInPlane = transform.position;
                personInPlane.y = preview.transform.position.y;
                preview.transform.rotation =
                    Quaternion.LookRotation(preview.transform.position - personInPlane, Vector3.up);

                x += 1;
                if (x >= width)
                {
                    y++;
                    x = 0;
                }
            }
        }

        private void Update()
        {
            foreach (var preview in previews)
            {
                var pos = controller.HeadPose.Pose.Value.Position;
                var distance = Vector3.Distance(pos, preview.transform.position);
                var gaussian = gaussianHeight *
                               Mathf.Exp(-gaussianWidth *
                                         Mathf.Pow(distance / maxCursorDistance, 2f));
                preview.SetIntensity(Mathf.Clamp01(gaussian));
            }
        }

        private FragmentTableModal modal;

        private void OnExitFragment(SteamVR_Action_Boolean fromaction,
                                    SteamVR_Input_Sources fromsource)
        {
            modal.Dismiss();
        }

        private void OnModalClosed()
        {
            Destroy(gameObject);
        }

        private void OnSelectFragment(SteamVR_Action_Boolean fromaction,
                                      SteamVR_Input_Sources fromsource)
        {
            var pos = controller.HeadPose.Pose.Value.Position;
            var closest = previews
                          .Where(preview => preview.IsFragmentValid)
                          .OrderBy(preview => Vector3.Distance(preview.transform.position, pos))
                          .First();
            if (Vector3.Distance(closest.transform.position, pos) > maxCursorDistance)
            {
                return;
            }

            modal.Dismiss();

            ToolManager.Instance.SetBuildFragmentTool(closest.Fragment);
        }

        private void OnDestroy()
        {
            selectFragmentAction.RemoveOnStateUpListener(OnSelectFragment, inputSources);
            exitFragmentTableAction.RemoveOnStateUpListener(OnExitFragment, inputSources);
        }

        [SerializeField]
        private float gaussianHeight;

        [SerializeField]
        private float gaussianWidth;

        [SerializeField]
        private float focalLength;

        [SerializeField]
        private float angularSpacing;

        [SerializeField]
        private float verticalSpacing;

        [SerializeField]
        private int width;

        public class FragmentTableModal : Modal
        {
            public FragmentTable Table { get; }

            public FragmentTableModal(ActionsManager manager, FragmentTable table) : base(manager)
            {
                Table = table;
            }

            public override bool HideMolecule => true;

            public override bool CanBeReplacedBy(IModal newModal)
            {
                return true;
            }

            public override void OnCloseModal()
            {
                Table.OnModalClosed();
            }
        }
    }
}