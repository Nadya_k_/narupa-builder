// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Linq;
using UnityEngine;

namespace NarupaBuilder
{
    public class VisualisationManager : MonoBehaviour
    {
        [SerializeField]
        private Transform simulationTransform;

        [SerializeField]
        private GameObject currentMoleculePrefab;

        [SerializeField]
        private GameObject hydrogenGhostPrefab;


        [SerializeField]
        private GameObject selectionMoleculeVisualiserPrefab;

        [SerializeField]
        private ActionsManager actions;

        /// <summary>
        /// Actual visualiser used to draw the currently assembled fragments
        /// </summary>
        private BuilderVisualisation realMoleculeVisualiser;


        /// <summary>
        /// Actual visualiser used to draw the currently assembled fragments
        /// </summary>
        private BuilderVisualisation transparentHydrogenVisualiser;

        private BuilderVisualisation selectionVisualiser;

        [SerializeField]
        private NarupaBuilder builder;


        public BuilderVisualisation BuildGhost => CreateVisualiser(visualisers.buildGhost);


        public GuidelinesVisualiser BuildGuidelinesValid
            => CreateGuidelineVisualiser(visualisers.buildGuidelinesValid);

        public GuidelinesVisualiser BuildGuidelinesInvalid
            => CreateGuidelineVisualiser(visualisers.buildGuidelinesInvalid);

        public IBuilderVisualiser BondOrderHighlight
            => CreateVisualiser(visualisers.bondOrderHighlight);

        public IBuilderVisualiser FragmentGhost => CreateVisualiser(visualisers.fragmentGhost);


        public IBuilderVisualiser ConnectGhost => CreateVisualiser(visualisers.connectGhost);


        public IBuilderVisualiser ConnectHighlight
            => CreateVisualiser(visualisers.connectHighlight);


        public IBuilderVisualiser DeleteHighlight => CreateVisualiser(visualisers.deleteHighlight);


        public IBuilderVisualiser MoveHighlight => CreateVisualiser(visualisers.moveHighlight);


        public IBuilderVisualiser RotateHighlight => CreateVisualiser(visualisers.rotateHighlight);

        public IBuilderVisualiser RotateHighlightAxis
            => CreateVisualiser(visualisers.rotateHighlightAxis);

        public IBuilderVisualiser SelectHighlight => CreateVisualiser(visualisers.selectHighlight);

        public IBuilderVisualiser DeselectHighlight
            => CreateVisualiser(visualisers.deselectHighlight);

        public static VisualisationManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        [Serializable]
        public class ActionVisualisers
        {
            [Header("Build")]
            [SerializeField]
            internal GameObject buildGhost;

            [SerializeField]
            internal GameObject buildGuidelinesValid;

            [SerializeField]
            internal GameObject buildGuidelinesInvalid;

            [Header("Bord Order")]
            [SerializeField]
            internal GameObject bondOrderHighlight;

            [Header("Build Fragment")]
            [SerializeField]
            internal GameObject fragmentGhost;

            [Header("Connect")]
            [SerializeField]
            internal GameObject connectGhost;

            [SerializeField]
            internal GameObject connectHighlight;

            [Header("Delete")]
            [SerializeField]
            internal GameObject deleteHighlight;

            [Header(("Move"))]
            [SerializeField]
            internal GameObject moveHighlight;

            [Header("Rotate")]
            [SerializeField]
            internal GameObject rotateHighlight;

            [SerializeField]
            internal GameObject rotateHighlightAxis;

            [Header("Select")]
            [SerializeField]
            internal GameObject selectHighlight;

            [SerializeField]
            internal GameObject deselectHighlight;
        }

        [SerializeField]
        private ActionVisualisers visualisers = new ActionVisualisers();

        private void Start()
        {
            realMoleculeVisualiser = CreateVisualiser(currentMoleculePrefab);
            transparentHydrogenVisualiser = CreateVisualiser(hydrogenGhostPrefab);
            selectionVisualiser = CreateVisualiser(selectionMoleculeVisualiserPrefab);
        }

        [SerializeField]
        private bool transparentHydrogens = true;

        private void Update()
        {
            var currentFrame = builder.EditableActiveFrame;
            var current = BondsAndParticles.DeepCopyUsingIndices(currentFrame);
            var selection = BondsAndParticles.DeepCopyUsingIndices(actions.Selection);

            selection.SetParticlePoseToMatchingParticlesPose(current.Particles);

            if (actions.IsActionPrepared)
                actions.CurrentAction.RenderActionPrepared(current, selection);
            else if (actions.IsActionInProgress)
                actions.CurrentAction.RenderActionInProgress(current, selection);
            current.ReindexBondsAndParticles();


            selection.SetParticlePoseToMatchingParticlesPose(current.Particles);
            selection.ReindexBondsAndParticles();

            var hideAll = actions.HasModal ? actions.CurrentModal.HideMolecule : false;

            if (!hideAll && transparentHydrogens)
            {
                var hydrogens = current.Particles.OfType<Atom>()
                                       .Hydrogens()
                                       .ToList();
                var toHideBonds = current.Bonds.BondsContainingAnyParticles(hydrogens).ToList();
                var nonHydrogens = current.Particles.Except(hydrogens).ToList();
                var nonHydrogenBonds = current.Bonds.Except(toHideBonds).ToList();
                realMoleculeVisualiser.SetBondsAndParticles(nonHydrogens, nonHydrogenBonds);
                transparentHydrogenVisualiser.SetBondsAndParticles(hydrogens, toHideBonds);
            }
            else
            {
                if (hideAll)
                {
                    realMoleculeVisualiser.ClearFrame();
                    transparentHydrogenVisualiser.SetBondsAndParticles(
                        current.Particles, current.Bonds);
                }
                else
                {
                    realMoleculeVisualiser.SetBondsAndParticles(current.Particles, current.Bonds);
                    transparentHydrogenVisualiser.ClearFrame();
                }
            }

            if (!hideAll)
                selectionVisualiser.SetFrame(selection);
            else
            {
                selectionVisualiser.ClearFrame();
            }
        }

        /// <summary>
        /// Instantiate the prefab to create a standard Narupa renderer, and wrap it in a
        /// <see cref="BuilderVisualisation" /> which will provide the frames.
        /// </summary>
        private BuilderVisualisation CreateVisualiser(GameObject prefab)
        {
            var visualiser = Instantiate(prefab, simulationTransform);
            var wrapper = new BuilderVisualisation();
            wrapper.SetNarupaVisualiser(visualiser);
            return wrapper;
        }

        private GuidelinesVisualiser CreateGuidelineVisualiser(GameObject prefab)
        {
            var visualiser = Instantiate(prefab, simulationTransform);
            var wrapper = new GuidelinesVisualiser();
            wrapper.SetNarupaVisualiser(visualiser);
            return wrapper;
        }

        public void ToggleHydrogens()
        {
            transparentHydrogens = !transparentHydrogens;
        }
    }
}