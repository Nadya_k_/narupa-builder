// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using UnityEngine;

namespace NarupaBuilder
{
    public interface IBuilderVisualiser
    {
        void SetFrame(BondsAndParticles frame);
        void SetBondsAndParticles(IEnumerable<Particle> particles, IEnumerable<Bond> bonds);
        void Destroy();
        void ClearFrame();

        void SetNarupaVisualiser(GameObject obj);
    }
}