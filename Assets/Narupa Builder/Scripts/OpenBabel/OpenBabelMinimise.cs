using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Narupa.Core.Science;
using OpenBabel;
using UnityEditor;

namespace NarupaBuilder.OpenBabel
{
    public static class OpenBabelMinimise
    {
        /// <summary>
        /// Minimise a structure using OpenBabel for a given number of steps
        /// </summary>
        public static OBMol MinimiseStructure(OBMol mol,
                                              int steps,
                                              string forcefieldName = "mmff94s")
        {
            var forcefield = GetForceField(forcefieldName, mol);

            forcefield.SteepestDescent(steps);

            if (!forcefield.GetCoordinates(mol))
                ThrowOpenBabelException("Failed to get conformer");

            return mol;
        }

        /// <summary>
        /// Minimise a structure continuously, calling onUpdate every 10 steps. Cancelling
        /// the task will finish the minimisation.
        /// </summary>
        public static async Task<BondsAndParticles> ContinuousMinimiseAsync(
            BondsAndParticles system,
            string forcefieldName,
            Action<BondsAndParticles> onUpdate,
            CancellationToken token)
        {
            if(!OpenBabelSetup.IsOpenBabelAvailable)
                OpenBabelSetup.ThrowOpenBabelNotFoundError();
            
            var mol = system.AsOBMol();
            var forcefield = GetForceField(forcefieldName, mol);

            while (!token.IsCancellationRequested)
            {
                forcefield.SteepestDescent(10);
                if (!forcefield.GetCoordinates(mol))
                    ThrowOpenBabelException("Failed to get conformer");
                onUpdate(mol.AsBondsAndParticles());
                try
                {
                    await Task.Delay(17, token);
                }
                catch (TaskCanceledException cancelled)
                {
                }
            }

            return mol.AsBondsAndParticles();
        }

        /// <summary>
        /// Attempt to find and setup the given forcefield for a given molecule.
        /// </summary>
        private static OBForceField GetForceField(string name, OBMol mol)
        {
            var forcefield = OBForceField.FindForceField(name);
            if (forcefield == null)
                ThrowOpenBabelException("Failed to load force field");
            if (!forcefield.Setup(mol))
                ThrowOpenBabelException("Failed to setup force field");
            return forcefield;
        }

        /// <summary>
        /// Throw an exception due to OpenBabel, looking in the error log to potentially
        /// find the issue.
        /// </summary>
        public static void ThrowOpenBabelException(string message)
        {
            var log = openbabel_csharp.obErrorLog;
            var error = "";
            var errors = log.GetMessagesOfLevel(OBMessageLevel.obError);
            if (errors.Count > 0)
                error = errors.Last();
            log.ClearLog();
            if (!string.IsNullOrEmpty(error))
                throw new OpenBabelException(message + "\n" + error);
            throw new OpenBabelException(message);
        }
    }

    /// <summary>
    /// Exception caused by an error in OpenBabel.
    /// </summary>
    public class OpenBabelException : Exception
    {
        public OpenBabelException(string message) : base(message)
        {
        }
    }
}