﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NarupaBuilder
{
    public class LineSegment
    {
        public Vector3 pointA, pointB;
        
        public LineSegment(Vector3 pointA, Vector3 pointB)
        {
            this.pointA = pointA;
            this.pointB = pointB;
        }
    }
}
