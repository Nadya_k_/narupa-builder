﻿// Copyright (c) Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace NarupaBuilder
{
    ///<summary> 
    /// Represents a sphere, consisting of a point in space (the origin) and a radius.
    ///</summary>
    public class Sphere
    {
        public Vector3 Origin { get; set; }
        public float Radius { get; set; }

        public Sphere(Vector3 origin, float radius)
        {
            Origin = origin;
            Radius = radius;
        }

        /// <summary>
        /// Determine if two spheres overlap
        /// </summary>
        public static bool DoOverlap(Sphere sphereA, Sphere sphereB)
        {
            var distance = Vector3.Distance(sphereA.Origin, sphereB.Origin);
            return distance < sphereA.Radius + sphereB.Radius;
        }

        /// <summary>
        /// Converts a Sphere from world space to local space.
        /// </summary>
        public Sphere InLocalSpace(Transform transform)
        {
            return new Sphere(transform.InverseTransformPoint(Origin),
                              Radius / transform.lossyScale.x);
        }
        public Sphere InLocalSpaceGlobalScale(Transform transform)
        {
            return new Sphere(transform.InverseTransformPoint(Origin),
                              Radius);
        }
    }
}