// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using UnityEngine;

namespace NarupaBuilder.Tools
{
    /// <summary>
    /// Base class for implementations of <see cref="ITool" /> that exist in a Unity
    /// project as a <see cref="ScriptableObject" />.
    /// </summary>
    public abstract class ScriptableObjectTool : ScriptableObject, ITool
    {
        /// <inheritdoc cref="ITool.DisplayName"/>
        public abstract string DisplayName { get; }

        /// <inheritdoc cref="ITool.SpriteIcon"/>
        public abstract Sprite SpriteIcon { get; }

        /// <inheritdoc cref="ITool.ControllerGizmo"/>
        public abstract GameObject ControllerGizmo { get; }

        /// <inheritdoc cref="ITool.StartAction"/>
        public abstract void StartAction();

        [SerializeField]
        private List<MenuItem> actions = new List<MenuItem>();

        /// <summary>
        /// Get associated menu items of this tool.
        /// </summary>
        public IReadOnlyList<MenuItem> AssociatedActions => actions;
    }
}