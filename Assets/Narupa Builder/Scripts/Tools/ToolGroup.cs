// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NarupaBuilder.Tools
{
    /// <summary>
    /// Wrapper around a group of one or more tools.
    /// </summary>
    /// <remarks>
    /// The current subtool is tracked using an unserialized member. This means a tool
    /// group cannot be used by multiple tool users at once.
    /// </remarks>
    [CreateAssetMenu(fileName = "New Tool Group", menuName = "Tool Group")]
    public class ToolGroup : ScriptableObjectTool, ITool
    {
        [SerializeField]
        private string displayName;

        [SerializeField]
        private Sprite image;

        [SerializeField]
        private GameObject toolEnd;

        public override string DisplayName => displayName ?? CurrentSubtool?.DisplayName;

        public override Sprite SpriteIcon => image ?? CurrentSubtool?.SpriteIcon;

        public override GameObject ControllerGizmo => toolEnd ?? CurrentSubtool?.ControllerGizmo;

        public override void StartAction()
        {
            CurrentSubtool?.StartAction();
        }

        [SerializeField]
        private List<Tool> tools = new List<Tool>();

        public IReadOnlyList<Tool> Tools => tools;

        private Tool currentSubtool;

        public Tool CurrentSubtool
        {
            get
            {
                if (currentSubtool == null)
                    currentSubtool = Tools.FirstOrDefault();
                return currentSubtool;
            }
        }

        public void SetSubtool(Tool subtool, bool triggerActionChange = true)
        {
            currentSubtool = subtool;
            if (triggerActionChange)
                StartAction();
        }
    }
}