// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NarupaBuilder.Tools
{
    /// <summary>
    /// A Tool is a state in which the builder can be in, which will ensure that the appropriate
    /// action is always prepared.
    /// </summary>
    public interface ITool
    {
        /// <summary>
        /// The display name of the tool, used in menus.
        /// </summary>
        string DisplayName { get; }

        /// <summary>
        /// The icon used to indicate the tool in a menu.
        /// </summary>
        Sprite SpriteIcon { get; }

        /// <summary>
        /// The prefab to indicate the current tool end.
        /// </summary>
        GameObject ControllerGizmo { get; }

        /// <summary>
        /// Callback when no builder action is prepared.
        /// </summary>
        void StartAction();
    }
}