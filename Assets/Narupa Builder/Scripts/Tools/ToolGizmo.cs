using System;
using Narupa.Frontend.Controllers;
using UnityEngine;
using UnityEngine.Assertions;

namespace NarupaBuilder.Tools
{
    /// <summary>
    /// Sets the gizmo of the tool.
    /// </summary>
    public class ToolGizmo : MonoBehaviour
    {
        [SerializeField]
        private ToolManager toolManager;
        
        [SerializeField]
        private ActionsManager actionsManager;

        [SerializeField]
        private VrController controller;

        [SerializeField]
        private ToolRunner toolRunner;

        private void Awake()
        {
            Assert.IsNotNull(toolManager);
            Assert.IsNotNull(actionsManager);
            Assert.IsNotNull(controller);
            Assert.IsNotNull(toolRunner);
            toolRunner.ToolSizeChanged += ToolRunnerOnToolSizeChanged;
            toolManager.ToolChanged += ToolManagerOnToolChanged;
        }

        private void ToolRunnerOnToolSizeChanged()
        {
            controller.SetCursorGizmoScale(toolRunner.RelativeToolSize);
        }

        private void ToolManagerOnToolChanged()
        {
            UpdateToolEnd(true);
        }

        private void Update()
        {
            UpdateToolEnd(false);
        }

        private void UpdateToolEnd(bool force = false)
        {
            if (controller.HasCursorGizmo != null && actionsManager.HasModal)
            {
                controller.InstantiateCursorGizmo(null);
                controller.SetCursorGizmoScale(toolRunner.RelativeToolSize);
            }
            else if ((!controller.HasCursorGizmo && !actionsManager.HasModal) || force)
            {
                var tool = toolManager.CurrentTool;
                var toolEnd = tool?.ControllerGizmo;
                controller.InstantiateCursorGizmo(toolEnd);
                controller.SetCursorGizmoScale(toolRunner.RelativeToolSize);
            }
        }
    }
}