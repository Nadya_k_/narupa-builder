// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using UnityEngine;

namespace NarupaBuilder.Tools
{
    public class BuildFragmentTool : ITool
    {
        public BuildFragmentTool(Fragment fragment)
        {
            Fragment = fragment;
            DisplayName = fragment.Name;
            SpriteIcon = null;
            ControllerGizmo = null;
            startAction = () => ActionsManager.Actions.StartBuildFragmentAction(fragment);
        }

        public Fragment Fragment { get; }

        public string DisplayName { get; }
        public Sprite SpriteIcon { get; }
        public GameObject ControllerGizmo { get; }

        private Action startAction;

        public void StartAction()
        {
            startAction();
        }

        public void OnStartTool()
        {
            
        }
    }
}