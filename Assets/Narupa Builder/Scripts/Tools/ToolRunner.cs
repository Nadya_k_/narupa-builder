// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Frontend.Controllers;
using Narupa.Frontend.XR;
using UnityEngine;
using Valve.VR;

namespace NarupaBuilder.Tools
{
    public class ToolRunner : MonoBehaviour
    {
        [SerializeField]
        private ToolManager toolManager;

        [SerializeField]
        private ActionsManager actionsManager;

        [SerializeField]
        private VrController controller;

        private BuilderController manipulator;

        [SerializeField]
        private float relativeToolSize = 0.5f;

        public float RelativeToolSize
        {
            get => relativeToolSize;
            set
            {
                relativeToolSize = value;
                ToolSizeChanged?.Invoke();
            }
        }

        [Header("Controller Actions")] [SerializeField]
        private SteamVR_Action_Boolean performAction;

        [SerializeField] private SteamVR_Input_Sources inputSource;

        public event Action ToolSizeChanged;

        private void Start()
        {
            manipulator = CreateBuilderController(inputSource);
            toolManager.ToolChanged += ToolManagerOnToolChanged;
        }

        private void ToolManagerOnToolChanged()
        {
            toolManager.CurrentTool.StartAction();
        }

        private BuilderController CreateBuilderController(SteamVR_Input_Sources source)
        {
            var builderController = new BuilderController
            {
                ToolPose = controller.CursorPose,
                InteractAction = performAction.WrapAsButton(source),
                Controller = controller,
                ToolRunner = this
            };
            return builderController;
        }

        private void Update()
        {
            if (actionsManager.IsActionAllowed && actionsManager.CurrentAction == null &&
                toolManager.CurrentTool != null)
            {
                actionsManager.SetActiveController(manipulator);
                toolManager.CurrentTool.StartAction();
            }
        }

        public Sphere GetWorldSpaceToolSphere()
        {
            return new Sphere(manipulator.ToolPose.Pose.Value.Position,
                              manipulator.ToolPose.Pose.Value.Scale.x *
                              RelativeToolSize);
        }
    }
}