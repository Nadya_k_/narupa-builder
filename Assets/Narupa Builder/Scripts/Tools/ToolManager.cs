// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NarupaBuilder.Tools
{
    /// <summary>
    /// Tracks the currently selected tool and subtool, as well as what the possible tools are.
    /// </summary>
    public class ToolManager : MonoBehaviour
    {
        private ITool currentTool;

        public ITool CurrentTool => currentTool;

        public bool HasTool => CurrentTool != null;
        public IReadOnlyList<ITool> Tools => tools;

        public event Action ToolChanged;

        [SerializeField]
        private List<ScriptableObjectTool> tools = new List<ScriptableObjectTool>();

        public static ToolManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            foreach (var tool in tools)
                if (tool is ToolGroup toolGroup)
                    toolGroup.SetSubtool(toolGroup.Tools.FirstOrDefault(), false);
        }

        public void SetTool(ITool tool)
        {
            currentTool = tool;
            ToolChanged?.Invoke();
        }

        [SerializeField]
        private ScriptableObjectTool buildFragmentTool;

        public void SetBuildFragmentTool(Fragment fragment)
        {
            BuildFragmentAction.CurrentFragment = fragment;
            SetTool(buildFragmentTool);
        }
    }
}