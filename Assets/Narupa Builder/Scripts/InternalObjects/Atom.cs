// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using Narupa.Core.Science;

namespace NarupaBuilder
{
    public class Atom : Particle
    {
        protected bool Equals(Atom other)
        {
            return base.Equals(other) && Element == other.Element;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Atom) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (int) Element;
            }
        }

        public override float Scale => ElementValues.GetParticleScale(Element);

        public override int CommonBondNumber
            => ElementValues.GetCommonBondNumber(Element);

        public IEnumerable<Atom> BondedAtoms => BondedParticles.OfType<Atom>();


        public override Particle Copy(bool scaledDown = false, uint? currentIndex = null)
        {
            return new Atom(Element, currentIndex ?? CurrentIndex, Pose, scaledDown || ScaledDown);
        }

        public Element Element { get; set; }

        public int CommonStericNumber => ElementValues.GetCommonStericNumber(Element);

        public Atom(Element element,
                    uint currentIndex,
                    PointTransformation pose,
                    bool scaledDown = false) : base(currentIndex, pose, scaledDown)
        {
            Element = element;
        }
    }
}