﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Linq;

namespace NarupaBuilder
{
    public class ConnectAction : BaseAction
    {
        private Particle sourceParticle = null;
        private Particle targetParticle = null;

        public ConnectAction(ActionsManager aM, VisualisationManager vM) : base(aM, vM)
        {
        }

        private IBuilderVisualiser ghostVisualiser;
        private IBuilderVisualiser highlightVisualiser;

        public override void OnActionPrepared()
        {
            base.OnActionPrepared();
            ghostVisualiser = Visualisations.ConnectGhost;
            highlightVisualiser = Visualisations.ConnectHighlight;
        }

        public override void UpdateActionPrepared()
        {
            base.OnActionStarted();
            sourceParticle = GetHighlightedParticlesInOrder().FirstOrDefault();
        }
        
        public override void UpdateActionInProgress()
        {
            base.UpdateActionInProgress();
            targetParticle = GetHighlightedParticlesInOrder().FirstOrDefault();
            if (targetParticle == sourceParticle)
                targetParticle = null;
        }

        private Bond GetExistingBond(Bond newBond, BondsAndParticles currentSystem)
        {
            return newBond.GetIdenticalBondInList(currentSystem.Bonds);
        }

        public override void OnActionFinished()
        {
            base.OnActionFinished();

            if (sourceParticle != null && targetParticle != null)
            {
                var newBonds = (new Bond(0, sourceParticle, targetParticle)).AsList();
                newBonds = newBonds.ReplaceBondedWithSameIndexParticles(CurrentSystem.Particles);
                foreach (var b in newBonds)
                {
                    var identicalBond = GetExistingBond(b, CurrentSystem);
                    if (identicalBond != null && identicalBond.BondOrder < 3)
                    {
                        identicalBond.BondOrder++;
                    }
                    else if (identicalBond != null)
                    {
                        CleanUp();
                        return;
                    }
                    else
                    {
                        CurrentSystem.Bonds.Add(b);
                    }
                }

                CurrentSystem.ReindexBondsAndParticles();
                CurrentSystem.UpdateBondedParticlesForParticles();
                Actions.SetSystemDirty();
            }

            CleanUp();
        }

        public override void OnActionCancelled()
        {
            base.OnActionCancelled();
            CleanUp();
        }

        public override void RenderActionPrepared(BondsAndParticles currentSystem,
                                                  BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            if (sourceParticle != null)
                highlightVisualiser.SetBondsAndParticles(sourceParticle.AsList(), new Bond[0]);
            else
                highlightVisualiser.ClearFrame();
        }

        public override void RenderActionInProgress(BondsAndParticles currentSystem,
                                                    BondsAndParticles currentSelection)
        {
            base.RenderActionInProgress(currentSystem, currentSelection);
            if (sourceParticle != null)
            {
                var particles = targetParticle != null
                                    ? new List<Particle> {sourceParticle, targetParticle}
                                    : sourceParticle.AsList();
                var bond = targetParticle != null
                               ? new List<Bond> {new Bond(0, sourceParticle, targetParticle)}
                               : new List<Bond>();

                if (bond.Count == 1)
                {
                    var existingBond = GetExistingBond(bond[0], CurrentSystem);
                    if (existingBond != null)
                    {
                        bond[0].BondOrder = existingBond.BondOrder + 1;
                    }
                }

                highlightVisualiser.SetBondsAndParticles(particles, bond);

                if (bond.Count == 1)
                    ghostVisualiser.SetBondsAndParticles(particles, bond);
                else
                    ghostVisualiser.ClearFrame();
            }
            else
                highlightVisualiser.ClearFrame();
        }

        private void CleanUp()
        {
            ghostVisualiser.Destroy();
            highlightVisualiser.Destroy();
        }
    }
}