// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Narupa.Core.Science;
using UnityEngine;

namespace NarupaBuilder.Tools
{
    [CreateAssetMenu(menuName = "BA", fileName = "BA")]
    public class BuilderActions : ScriptableObject
    {
        public void StartConnectAction()
        {
            StartAction(new ConnectAction(ActionsManager.Instance, VisualisationManager.Instance));
        }

        public void StartBondOrderAction()
        {
            StartAction(new BondOrderAction(ActionsManager.Instance,
                                            VisualisationManager.Instance));
        }

        public void StartDeleteAction()
        {
            StartAction(new DeleteAction(ActionsManager.Instance, VisualisationManager.Instance));
        }

        public void StartMoveAction()
        {
            StartAction(new MoveAction(ActionsManager.Instance, VisualisationManager.Instance));
        }

        public void StartRotateAction()
        {
            StartAction(new RotateAction(ActionsManager.Instance, VisualisationManager.Instance));
        }

        public void StartSelectAction(SelectModes type)
        {
            StartAction(new SelectAction(ActionsManager.Instance, VisualisationManager.Instance,
                                         type));
        }

        public void StartBuildAtomAction(Element element)
        {
            StartAction(new BuildAction(ActionsManager.Instance, VisualisationManager.Instance,
                                        element));
        }

        public void StartBuildAtomAction(int atomicNumber)
        {
            StartBuildAtomAction((Element) atomicNumber);
        }

        public void StartBuildFragmentAction()
        {
            if (BuildFragmentAction.CurrentFragment != null)
                StartBuildFragmentAction(BuildFragmentAction.CurrentFragment);
        }

        public void StartSelectSingleAction()
        {
            StartAction(new SelectAction(ActionsManager.Instance, VisualisationManager.Instance,
                                         SelectModes.SelectSingle));
        }

        public void StartSelectFragmentAction()
        {
            StartAction(new SelectAction(ActionsManager.Instance, VisualisationManager.Instance,
                                         SelectModes.SelectFragment));
        }

        private void StartAction(IAction action)
        {
            ActionsManager.Instance.StartAction(action);
        }

        public void StartBuildFragmentAction(Fragment fragment)
        {
            StartAction(
                new BuildFragmentAction(ActionsManager.Instance, 
                                        VisualisationManager.Instance,
                                        fragment));
        }


        [SerializeField]
        private FragmentTable fragmentTablePrefab;

        public void ShowFragmentTable(string folder)
        {
            var fragmentTable = Instantiate(fragmentTablePrefab);
            fragmentTable.Setup(ActionsManager.Instance, FragmentDictionary.GetStreamingAssetsFragments(folder));
        }

        public void ShowFragmentTable(IReadOnlyCollection<IFragmentProvider> fragments)
        {
            var fragmentTable = Instantiate(fragmentTablePrefab);
            fragmentTable.Setup(ActionsManager.Instance, fragments);
        }

        public void DeselectAll()
        {
            ActionsManager.Instance.ClearSelection();
        }

        public void CopySelection()
        {
            var copy = ActionsManager.Instance.GetCopyOfSelection();
            ToolManager.Instance.SetBuildFragmentTool(new Fragment("Copy", copy));
        }
    }
}