﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Linq;

namespace NarupaBuilder
{
    public class DeleteAction : BaseAction
    {
        private BondsAndParticles toDelete = new BondsAndParticles();

        public DeleteAction(ActionsManager aM, VisualisationManager vM) : base(aM, vM)
        {
        }

        private IBuilderVisualiser highlight;

        public override void OnActionPrepared()
        {
            base.OnActionPrepared();
            highlight = Visualisations.DeleteHighlight;
        }

        public override void UpdateActionPrepared()
        {
            base.UpdateActionPrepared();
            toDelete.Particles = GetHighlightedParticles().ToList();
            toDelete.Bonds = GetHighlightedBondsInOrder();
            toDelete.Bonds.AddRange(CurrentSystem.Bonds.BondsContainingAnyParticles(toDelete.Particles));

        }

        public override void UpdateActionInProgress()
        {
            base.UpdateActionInProgress();
            toDelete.Particles.AddRange(GetHighlightedParticlesInOrder());
            toDelete.Bonds.AddRange(GetHighlightedBondsInOrder());
            toDelete.Bonds.AddRange(CurrentSystem.Bonds.BondsContainingAnyParticles(toDelete.Particles));
            toDelete.RemoveDuplicates();
            
        }

        public override void OnActionFinished()
        {
            base.OnActionFinished();
            if (!toDelete.IsEmpty)
            {
                toDelete.Bonds.AddRange(
                    CurrentSystem.Bonds.BondsContainingAnyParticles(toDelete.Particles));
                CurrentSystem.RemoveParticlesAndBondsByIndex(toDelete);
                CurrentSystem.ReindexBondsAndParticles();
                CurrentSystem.UpdateBondedParticlesForParticles();
                Actions.SetSystemDirty();
            }

            ClearUp();
        }

        public override void OnActionCancelled()
        {
            base.OnActionCancelled();
            ClearUp();
        }

        public override void RenderActionInProgress(BondsAndParticles currentSystem,
                                                    BondsAndParticles currentSelection)
        {
            base.RenderActionInProgress(currentSystem, currentSelection);
            RenderAction();
        }

        public override void RenderActionPrepared(BondsAndParticles currentSystem,
                                                  BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            RenderAction();
        }

        private void RenderAction()
        {
            highlight.SetBondsAndParticles(toDelete.Particles,toDelete.Bonds);
        }

        private void ClearUp()
        {
            highlight.Destroy();
        }
    }
}