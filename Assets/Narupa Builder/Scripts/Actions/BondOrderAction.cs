﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Linq;
using Narupa.Core.Math;
using UnityEngine;

namespace NarupaBuilder
{
    public class BondOrderAction : BaseAction
    {
        private Transformation startPose;
        private Transformation currentPose;
        private Bond toChange = null;
        private int newOrder;

        public BondOrderAction(ActionsManager aM, VisualisationManager vM) : base(aM, vM)
        {
        }

        private IBuilderVisualiser visualiser;

        public override void OnActionPrepared()
        {
            base.OnActionPrepared();
            visualiser = Visualisations.BondOrderHighlight;
        }

        public override void UpdateActionPrepared()
        {
            base.UpdateActionPrepared();
            toChange = GetHighlightedBondsInOrder().FirstOrDefault();
        }

        public override void OnActionStarted()
        {
            base.OnActionStarted();
            startPose = WorldSpacePose;
        }

        public override void UpdateActionInProgress()
        {
            base.UpdateActionInProgress();
            if (toChange != null)
            {
                currentPose = WorldSpacePose;
                newOrder = 1 + (int) Mathf.Repeat(toChange.BondOrder + 0.001f, 3);
            }
        }

        public override void OnActionFinished()
        {
            base.OnActionFinished();
            if (toChange != null && toChange.BondOrder != newOrder)
            {
                toChange.BondOrder = newOrder;
                CurrentSystem.ReindexBondsAndParticles();
                CurrentSystem.UpdateBondedParticlesForParticles();
                Actions.SetSystemDirty();
            }

            ClearUp();
        }

        public override void OnActionCancelled()
        {
            base.OnActionCancelled();
            ClearUp();
        }

        private void ClearUp()
        {
            visualiser.Destroy();
        }

        public override void RenderActionPrepared(BondsAndParticles currentSystem,
                                                  BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            if (toChange != null)
                visualiser.SetBondsAndParticles(toChange.BondedParticles, toChange.AsList());
            else
                visualiser.ClearFrame();
        }

        public override void RenderActionInProgress(BondsAndParticles currentSystem,
                                                    BondsAndParticles currentSelection)
        {
            base.RenderActionInProgress(currentSystem, currentSelection);
            if (toChange != null)
            {
                visualiser.SetBondsAndParticles(toChange.BondedParticles, toChange.AsList());
                currentSystem.Bonds[(int) toChange.CurrentIndex].BondOrder = newOrder;
            }
            else
                visualiser.ClearFrame();
        }
    }
}