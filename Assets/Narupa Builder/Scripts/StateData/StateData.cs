﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace NarupaBuilder
{
    public class StateData
    {
        private int CurrentStateIndex = 0;
        private List<BondsAndParticles> stateHistory = new List<BondsAndParticles>(new BondsAndParticles[] { new BondsAndParticles() });
        public BondsAndParticles CurrentState => stateHistory[CurrentStateIndex];

        public void AddFrameAsLastInHistory(BondsAndParticles frame)
        {
            var copy = BondsAndParticles.DeepCopy(frame);
            stateHistory = stateHistory.Take(CurrentStateIndex + 1).ToList();
            stateHistory.Add(copy);
            CurrentStateIndex = stateHistory.Count - 1;
        }

        public void ExecuteUndo()
        {
            if (CurrentStateIndex > 0)
            {
                CurrentStateIndex--;
            }
        }

        public void ExecuteRedo()
        {
            if (CurrentStateIndex < stateHistory.Count - 1)
            {
                CurrentStateIndex++;
            }
        }

    }
}
