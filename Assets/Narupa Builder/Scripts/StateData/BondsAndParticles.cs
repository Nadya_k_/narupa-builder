﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Science;
using Narupa.Frame;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// Collection of both a set of particles and a set of bonds.
    /// </summary>
    public class BondsAndParticles
    {
        public List<Particle> Particles { get; set; } = new List<Particle>();

        public List<Bond> Bonds { get; set; } = new List<Bond>();

        public IEnumerable<Atom> Atoms => Particles.OfType<Atom>();

        public bool IsEmpty => Bonds.Count == 0 && Particles.Count == 0;

        public BondsAndParticles()
        {
        }

        /// <summary>
        /// Copies a system, returning a copy of each particle and a copy of each bond,
        /// which now point to a matching particle in the new list.
        /// </summary>
        public static BondsAndParticles DeepCopy(BondsAndParticles bNP)
        {
            var copy = new BondsAndParticles();

            copy.Particles = bNP.Particles
                                .Select(particle => particle.Copy())
                                .ToList();

            copy.Bonds = bNP.Bonds
                            .Select(bond => new Bond(bond.CurrentIndex,
                                                     copy.Particles
                                                         [bNP.Particles.IndexOf(bond.A)],
                                                     copy.Particles
                                                         [bNP.Particles.IndexOf(bond.B)],
                                                     bond.BondOrder))
                            .ToList();

            copy.UpdateBondedParticlesForParticles();
            return copy;
        }

        /// <summary>
        /// Copies a system, returning a copy of each particle and a copy of each bond,
        /// which now point to the particle in the new list with the corresponding
        /// index.
        /// </summary>
        public static BondsAndParticles DeepCopyUsingIndices(BondsAndParticles bNP)
        {
            var copy = new BondsAndParticles();

            copy.Particles = bNP.Particles
                                .Select(particle => particle.Copy())
                                .ToList();

            copy.Bonds = bNP.Bonds
                            .Select(bond => new Bond(bond.CurrentIndex,
                                                     copy.Particles[(int) bond.A
                                                                              .CurrentIndex],
                                                     copy.Particles[(int) bond.B
                                                                              .CurrentIndex],
                                                     bond.BondOrder))
                            .ToList();

            copy.UpdateBondedParticlesForParticles();
            return copy;
        }

        /// <summary>
        /// Create a new <see cref="BondsAndParticles" /> containing the SAME bonds and
        /// particles (no copy is taken).
        /// </summary>
        public BondsAndParticles(BondsAndParticles bondsAndParticles)
        {
            Bonds.AddRange(bondsAndParticles.Bonds);
            Particles.AddRange(bondsAndParticles.Particles);
        }

        public BondsAndParticles(IEnumerable<Bond> bonds, IEnumerable<Particle> particles)
        {
            Bonds.AddRange(bonds);
            Particles.AddRange(particles);
        }

        public void Clear()
        {
            Bonds.Clear();
            Particles.Clear();
        }

        public void RemoveDuplicates()
        {
            Bonds = Bonds.Distinct().ToList();
            Particles = Particles.Distinct().ToList();
        }

        public void Except(BondsAndParticles bNP)
        {
            Bonds = Bonds.Except(bNP.Bonds).ToList();
            Particles = Particles.Except(bNP.Particles).ToList();
        }

        public void RemoveParticlesAndBondsByIndex(BondsAndParticles removals) //can do in BaP
        {
            foreach (var toRemove in removals.Particles)
            {
                Particles = Particles.Where(p => p.CurrentIndex != toRemove.CurrentIndex).ToList();
            }

            foreach (var toRemove in removals.Bonds)
            {
                Bonds = Bonds.Where(b => b.CurrentIndex != toRemove.CurrentIndex).ToList();
            }
        }

        public void RemoveParticlesByIndex(IEnumerable<Particle> removals) //can do in BaP
        {
            foreach (var toRemove in removals)
            {
                Particles = Particles.Where(p => p.CurrentIndex != toRemove.CurrentIndex).ToList();
            }
        }

        public void RemoveParticlesAndRelatedBondsByIndex(IReadOnlyCollection<Particle> removals)
        {
            var list = new List<Particle>();
            foreach (var particle in Particles)
            {
                var found = false;
                foreach (var removal in removals)
                {
                    if (particle.CurrentIndex == removal.CurrentIndex)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                    list.Add(particle);
            }

            Particles = list;

            var bondList = new List<Bond>();
            foreach (var bond in Bonds)
            {
                var found = false;
                foreach (var removal in removals)
                {
                    if (bond.A.CurrentIndex == removal.CurrentIndex ||
                        bond.B.CurrentIndex == removal.CurrentIndex)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                    bondList.Add(bond);
            }

            Bonds = bondList;
        }

        public void RemoveBondsByIndex(IEnumerable<Bond> removals) //can do in BaP
        {
            foreach (var toRemove in removals)
            {
                Bonds = Bonds.Where(b => b.CurrentIndex != toRemove.CurrentIndex).ToList();
            }
        }

        public void ReindexBondsAndParticles()
        {
            for (var i = 0; i < Particles.Count; i++)
            {
                Particles[i].CurrentIndex = (uint) i;
            }

            for (var i = 0; i < Bonds.Count; i++)
            {
                Bonds[i].CurrentIndex = (uint) i;
            }
        }

        public void UpdateBondedParticlesForParticles()
        {
            foreach (var particle in Particles)
            {
                particle.BondedParticles.Clear();
                particle.MyBonds.Clear();
            }

            foreach (var bond in Bonds)
            {
                foreach (var bondedParticle in bond.BondedParticles)
                {
                    bondedParticle.MyBonds.Add(bond);
                    bondedParticle.BondedParticles.Add(bond.Other(bondedParticle));
                }
            }
        }

        public void SetParticlePoseToMatchingParticlesPose(IEnumerable<Particle> newPoses)
        {
            for (var i = 0; i < Particles.Count; i++)
            {
                foreach (var newP in newPoses)
                {
                    if (Particles[i].CurrentIndex == newP.CurrentIndex)
                    {
                        Particles[i].Pose = newP.Pose;
                    }
                }
            }
        }

        public void RecenterParticles(Vector3 newCenter, bool excludeHydrogens = false)
        {
            Vector3 oldCenter;
            if (excludeHydrogens)
            {
                oldCenter = Atoms.ExceptHydrogens()
                                 .Select(p => p.Position)
                                 .Average();
            }
            else
            {
                oldCenter = Atoms.Select(p => p.Position)
                                 .Average();
            }

            var shift = newCenter - oldCenter;

            Particles.ForEach(p => p.Position += shift);
        }

        public Particle GetParticleWithSameIndex(Particle other)
        {
            return Particles.FirstOrDefault(p => p.CurrentIndex == other.CurrentIndex);
        }

        public Particle GetParticleWithSamePosition(Particle other)
        {
            return Particles.FirstOrDefault(p => p.Pose.Position == other.Pose.Position);
        }

        public List<BondsAndParticles> BreakIntoResiduesAndOrder()
        {
            var residues = new List<BondsAndParticles>();
            var full = DeepCopy(this);
            var depth = 0;
            while (full.Particles.Count > 0 && depth < full.Particles.Count * 3)
            {
                var residueUnsorted = full.Particles[0]
                                          .GetFragmentFromParticle(full.Particles.Count);
                var residueParticles =
                    residueUnsorted.OrderBy(particle => (particle as Atom).Element).ToList();
                residueParticles.Reverse();
                var k = 0;
                foreach (var p in residueParticles)
                {
                    p.CurrentIndex = (uint) k;
                    k++;
                }

                k++;
                var residueBonds = new List<Bond>();
                residueBonds = full.Bonds.Select(b => b)
                                   .Where(b => residueParticles.Contains(b.A) ||
                                               residueUnsorted.Contains(b.B))
                                   .ToList();

                residues.Add(
                    new BondsAndParticles(
                        new BondsAndParticles(residueBonds,
                                              residueParticles
                                                  .ToList())));
                full.Particles = full.Particles.Except(residueParticles).ToList();
                depth++;
            }

            return residues;
        }

        public Frame GetNarupaFrame() //delete?
        {
            var frame = new Frame();
            frame.ParticlePositions = new Vector3[Particles.Count];


            frame.ParticleElements = new Element[Particles.Count];
            frame.BondPairs = new BondPair[Bonds.Count];
            frame.ParticlePositions = Particles
                                      .Select(particle => particle.Pose.Position)
                                      .ToArray();

            frame.ParticleElements = Particles
                                     .Select(particle => ((Atom) particle).Element)
                                     .ToArray();

            frame.BondPairs = Bonds
                              .Select(bond => new BondPair(
                                          (int) bond.A.CurrentIndex,
                                          (int) bond.B.CurrentIndex))
                              .ToArray();

            frame.BondOrders = Bonds.Select(bond => bond.BondOrder).ToArray();
            return frame;
        }

        public void BalanceHydrogens()
        {
            var toRemoveHydsAndRelatedBonds = new BondsAndParticles();
            foreach (var a in Atoms.Hydrogens()
                                   .Where(a => a.BondedAtoms.Count() == 1 &&
                                               a.BondedAtoms.All(bonded => bonded.Element
                                                                        != Element.Hydrogen)))
            {
                toRemoveHydsAndRelatedBonds.Particles.Add(a);
            }

            toRemoveHydsAndRelatedBonds
                .Bonds.AddRange(Bonds.BondsContainingAnyParticlesByIndex(toRemoveHydsAndRelatedBonds
                                                                             .Particles));
            Except(toRemoveHydsAndRelatedBonds);
            ReindexBondsAndParticles();
            UpdateBondedParticlesForParticles();
            var guidelineClusters = Guidelines.CalculateGuidelines(Atoms);
            foreach (var gD in guidelineClusters)
            {
                var idealBondLength = ElementValues.GetIdealBondLength(gD.core.Element,
                                                                       Element.Hydrogen,
                                                                       1);

                gD.positions = gD.positions.Select(v => v * idealBondLength).ToList();
                var hydPositions = gD.positions.Take(Guidelines.EmptyBondNumber(gD.core)).ToList();
                foreach (var position in hydPositions)
                {
                    Particle hydrogen =
                        new Atom(Element.Hydrogen, 0, gD.core.Pose + position);
                    var hydToCore = new Bond(0, hydrogen, gD.core, 1);
                    Particles.Add(hydrogen);
                    Bonds.Add(hydToCore);
                }
            }

            ReindexBondsAndParticles();
            UpdateBondedParticlesForParticles();
        }


        /// <summary>
        /// Return a copy of the system with only bonds between the particles present.
        /// </summary>
        public BondsAndParticles CopyWithConnectingBonds()
        {
            var toExportBonds = new HashSet<Bond>();
            foreach (var p in Particles)
            {
                foreach (var b in p.MyBonds)
                {
                    var other = b.Other(p);
                    if (other.CurrentIndex < p.CurrentIndex)
                        continue;
                    if (Particles.Contains(other))
                        toExportBonds.Add(b);
                }
            }

            var toExportBondsCopy = toExportBonds.ToList().DeepCopyBondList();
            var toExportParticlesCopy =
                Particles.DeepCopyParticleListWithoutConnections();
            toExportBondsCopy =
                toExportBondsCopy.ReplaceBondedWithSameIndexParticles(toExportParticlesCopy);
            return new BondsAndParticles(toExportBondsCopy, toExportParticlesCopy);
        }
    }
}