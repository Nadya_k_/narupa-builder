using System;

namespace Plugins.Narupa.Core
{
    public static class StringExtensions
    {
        public static bool EqualsInvariantCultureIgnoreCase(this string a, string other)
        {
            return a.Equals(other, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}